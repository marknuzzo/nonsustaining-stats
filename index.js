const config = require("./config.json");

const chain = async () => {
  const issuesTotals = await require("./steps/issuesTotals")(config);
  const milestones = await require("./steps/milestonesToDates")(config);
  const issuesRates = await require("./steps/issuesRates")(config, milestones);
  const completionRates = await require("./steps/completionRates")(config);
  require("./steps/output")(issuesTotals, issuesRates, completionRates);
};

chain();
