const axios = require("axios").default;

/**
 * Get the ISO 8601 formatted dates for each of the given milestones
 */

module.exports = config => {
  return Promise.all(
    Object.keys(config.milestones).map(async milestone => {
      const response = await axios({
        method: "get",
        url: `https://gitlab.com/api/v4/groups/${config.groupId}/milestones?title=${milestone}`,
        headers: {
 //           "PRIVATE-TOKEN": config.gitlabPrivateToken
          "PRIVATE-TOKEN": process.env.GITLAB_NST_TOKEN
        }
      });

      return {
        title: milestone,
        startDate: new Date(response.data[0].start_date).toISOString(),
        endDate: new Date(response.data[0].due_date).toISOString(),
      };
    })
  );
};
