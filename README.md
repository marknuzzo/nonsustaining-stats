# Non-Sustaining stats

A script to fetch the GitLab API for data on the past year of "sustaining" issues.

1. Run `yarn install` to install required node modules
1. Create a `config.json` file based on the `config-example.json`
1. Run `yarn run start` to run the script
1. `output.md` will be generated for you upon completion
